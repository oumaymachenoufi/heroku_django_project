import { Injectable } from '@angular/core';

const TOKEN_KEY = 'auth_token';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor() { }

  saveToken(token: string) {
    window.localStorage.setItem(TOKEN_KEY, token)
    console.log(token)
  }

  getToken(): string | null {
    let token = window.localStorage.getItem(TOKEN_KEY)
    console.log({ 'get-token': 'token' });
    return token
  }

  removeToken() {
    window.localStorage.clear()
  }
}
