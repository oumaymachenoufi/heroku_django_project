import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentService {


  studentsListSubject = new Subject<any[]>();


  constructor() { }


  emitStudentsSubject() {
    this.studentsListSubject.next(this.studentsList.slice())
  }

  private studentsList = [
    {
      prenom: 'Mélanie',
      familyName: 'Diligent',
      age: 25,
      hobbies: ['Surf', 'Sport', 'lecture'],
      presence: true,
    },
    {
      prenom: 'Fréderic',
      familyName: 'Duarte',
      age: 24,
      hobbies: ['Ecriture', 'Chant', 'lecture'],
      presence: false,

    },
    {
      prenom: 'Milho',
      familyName: 'De Oliveira',
      age: 23,
      hobbies: ['Voyage', 'Chant', 'lecture'],
      presence: false,


    },
    {
      prenom: 'Oumayma',
      familyName: 'Channoufi',
      age: 25,
      hobbies: ['Ecriture', 'Dessin', 'lecture'],
      presence: true,


    }
  ]

  getStudentById(id: number): any {

    return this.studentsList[id];

  }


  enableOne(i: number) {
    console.log(i)
    console.log('enable element in service')
    this.studentsList[i].presence = true;
    this.emitStudentsSubject();

  }

  disableOne(i: number) {
    console.log('disable element in service')
    this.studentsList[i].presence = false;
    this.emitStudentsSubject();
  }


  addStudentToList(student: any) {
    this.studentsList.push(student);
    this.emitStudentsSubject();
  }

}
