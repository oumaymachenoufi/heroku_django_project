import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClassroomService {


  classroomListSubject = new Subject<any[]>();


  private classroomList = [
    { name: 'JAVA', studentsNum: 12, status: true },
    { name: 'Angular', studentsNum: 10, status: false },
    { name: 'Python', studentsNum: 5, status: true },
    { name: 'Django', studentsNum: 22, status: false },
    { name: 'Flutter', studentsNum: 12, status: true },
  ]

  emitClassroomSubject() {
    this.classroomListSubject.next(this.classroomList.slice())
  }

  disableAll() {

    for (let classoom of this.classroomList) {
      classoom.status = false;
    }
    this.emitClassroomSubject();

  }

  enableOne(i: number) {
    console.log(i)
    console.log('enable element in service')
    this.classroomList[i].status = true;
    this.emitClassroomSubject();


  }

  disableOne(i: number) {
    console.log('disable element in service')
    this.classroomList[i].status = false;
    this.emitClassroomSubject();

  }

  enableAll() {
    for (let classoom of this.classroomList) {
      classoom.status = true;
    }
    this.emitClassroomSubject();
  }

  getClassroomById(id: number): any {
    return this.classroomList[id];
  }

  getClassroomByName(name: String): any {
    const classroom = this.classroomList.find((element) => {
      return element.name === name;
    })
    return classroom;
  }

  addClassroomToList(classroom: any) {
    this.classroomList.push(classroom);
    this.emitClassroomSubject();
  }



  constructor() { }
}
