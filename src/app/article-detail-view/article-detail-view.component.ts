import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { RemoteArticlesService } from '../services/remote-articles.service';

@Component({
  selector: 'app-article-detail-view',
  templateUrl: './article-detail-view.component.html',
  styleUrls: ['./article-detail-view.component.css']
})
export class ArticleDetailViewComponent implements OnInit {



  article: any;
  articleSubscription!: Subscription;
  id: string = '';


  constructor(private articlesService: RemoteArticlesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id']

    this.articleSubscription = this.articlesService.singleArticleSubject.subscribe(
      (article: any) => {
        this.article = article;
      }
    )
    this.articlesService.getArticleFromServerById(Number(this.id));
  }

}
