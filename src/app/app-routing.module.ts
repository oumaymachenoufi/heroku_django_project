import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArticleDetailViewComponent } from './article-detail-view/article-detail-view.component';
import { ArticlesViewComponent } from './articles-view/articles-view.component';
import { AuthComponent } from './auth/auth.component';
import { ClassroomDetailsComponent } from './classroom-details/classroom-details.component';
import { ClassroomViewComponent } from './classroom-view/classroom-view.component';
import { EditClassroomComponent } from './edit-classroom/edit-classroom.component';
import { NewStudentComponent } from './new-student/new-student.component';
import { AuthGuardService } from './services/auth-guard.service';
import { StudentDetailsViewComponent } from './student-details-view/student-details-view.component';
import { StudentsViewComponent } from './students-view/students-view.component';

const routes: Routes = [
  { path: 'classes', component: ClassroomViewComponent },
  { path: 'auth', component: AuthComponent },
  { path: 'students', canActivate: [AuthGuardService], component: StudentsViewComponent },
  { path: 'classes/:id', component: ClassroomDetailsComponent },
  { path: 'students/:id', canActivate: [AuthGuardService], component: StudentDetailsViewComponent },
  { path: 'articles/:id', component: ArticleDetailViewComponent },
  { path: 'edit-classroom', component: EditClassroomComponent },
  { path: 'new-student', component: NewStudentComponent },
  { path: 'articles', component: ArticlesViewComponent }




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
