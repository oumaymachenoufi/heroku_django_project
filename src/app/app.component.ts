import { Component, OnDestroy, OnInit } from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { RemoteArticlesService } from './services/remote-articles.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  seconds: number = 0;
  counterSubscription!: Subscription;

  constructor() {

  }

  ngOnDestroy(): void {
    this.counterSubscription.unsubscribe();
  }



  ngOnInit() {


    const counter = interval(1000);

    this.counterSubscription = counter.subscribe(
      (value) => {
        this.seconds = value;
      },
      (error) => {
        console.log('there was an error' + error);
      },
      () => {
        console.log('Observable completed');
      }
    )


  }

  title = 'first-project2';
  isAuth = false;







}
