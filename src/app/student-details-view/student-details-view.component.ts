import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StudentService } from '../services/student.service';

@Component({
  selector: 'app-student-details-view',
  templateUrl: './student-details-view.component.html',
  styleUrls: ['./student-details-view.component.css']
})
export class StudentDetailsViewComponent implements OnInit {

  constructor(private route: ActivatedRoute, private studentService: StudentService) { }

  id: String = ''


  student: any = {
    prenom: 'Fréderic',
    familyName: 'Duarte',
    age: 24,
    hobbies: ['Ecriture', 'Chant', 'lecture'],
    presence: false,
  };

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id']
    this.student = this.studentService.getStudentById(+this.id);
  }

}
