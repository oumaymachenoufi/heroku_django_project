import { Component, Input, OnInit } from '@angular/core';
import { ClassroomService } from '../services/classroom.service';

@Component({
  selector: 'app-classroom',
  templateUrl: './classroom.component.html',
  styleUrls: ['./classroom.component.css']
})
export class ClassroomComponent implements OnInit {


  constructor(private classroomService: ClassroomService) {

  }

  ngOnInit(): void {
  }

  @Input()
  classroomName: string = "Django et Angular";

  @Input()
  numberStudents: Number = 12

  @Input()
  status: boolean = false;

  @Input()
  color: string = 'green';

  @Input()
  index!: number;

  lastUpdate = new Date();


  fontSize = 18;

  getColor(): string {
    return this.status ? 'green' : 'red'
  }

  getStyle(): any {
    return {
      'color': this.getColor(),
      'font-size': this.fontSize + 'px'
    }
  }



  disable() {
    console.log('disable element')
    this.classroomService.disableOne(this.index);
    this.status = false;
  }

  enable() {
    this.classroomService.enableOne(this.index)
    this.status = true

  }






}
