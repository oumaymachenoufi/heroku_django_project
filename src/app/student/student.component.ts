import { Component, Input, OnInit } from '@angular/core';
import { StudentService } from '../services/student.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  constructor(private studentService: StudentService) { }

  @Input()
  firstName: String = "Jean";

  @Input()
  familyName: String = "Dujardin";

  @Input()
  age: Number = 99;

  @Input()
  presence = true

  @Input()
  hobbiesList: String[] = ["Course", "Peinture", "Musique"]

  @Input()
  indexNumber: number = 0;



  changeToAbsent() {
    this.studentService.disableOne(this.indexNumber)
  }

  getColor() {
    if (this.presence === true) {
      return 'green';
    } else {
      return 'red'
    }
  }

  getStyle(): any {
    if (this.presence === true) {
      return { 'background-color': 'green' };
    } else {
      return { 'background-color': 'red' };
    }
  }


  changeToPresent() {
    this.studentService.enableOne(this.indexNumber)
  }

  ngOnInit(): void {
  }

}
