import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { StudentService } from '../services/student.service';

@Component({
  selector: 'app-students-view',
  templateUrl: './students-view.component.html',
  styleUrls: ['./students-view.component.css']
})
export class StudentsViewComponent implements OnInit, OnDestroy {
  studentsList: any = []
  studentsSubscription!: Subscription

  constructor(private studentService: StudentService) { }
  ngOnDestroy(): void {
    this.studentsSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.studentsSubscription = this.studentService.studentsListSubject.subscribe(
      (students_list: any[]) => {
        this.studentsList = students_list;
      }
    )
    this.studentService.emitStudentsSubject();
  }

}
